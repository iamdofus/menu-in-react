let Menu = React.createClass({
    render: function(){
        let menuItems = ['home','services','Dom','contact us','Samsung'];
        return <div>
            <nav>
            {menuItems.map(function(itemsInit, index){
                return <li key={index}><Link label={itemsInit}/></li>
            })}
             </nav>
        </div>
    }
});

let Link = React.createClass({
    render: function(){
        let changeLinkToReadable = this.props.label.toLowerCase().trim().replace(' ', '-');
        let url = "/"+changeLinkToReadable;
        return <a href={url}>{this.props.label}</a>
    }
});

ReactDOM.render(
    <Menu />,
    document.getElementById('div1')
);
